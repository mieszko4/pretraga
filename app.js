angular.module('project', ['ngRoute', 'ngResource'])

.factory('Projects', function($resource) {
  // return $resource('https://prigovor.hr/api/pretraga');
  return $resource('list.json');
})

.config(function($routeProvider) {
  $routeProvider
    .when('/', {
      controller:'ProjectListController as projectList',
      templateUrl:'list.html'
    })
    .otherwise({
      redirectTo:'/'
    });
})

.controller('ProjectListController', function(Projects) {
  var projectList = this;
  projectList.projects = []
  projectList.submit = function () {
    projectList.projects = Projects.query({ q: projectList.query });
  };
});
